const config = {
	apps: [
		{
			name: 'donetsk-ru:3005',
			script: './index.js',
			error: './logs/pm2.err',
			instances: 1,
			autorestart: true,
			watch: false,
			max_memory_restart: '1G',
			env_production: {
				PORT: '3005',
				NODE_ENV: 'production'
			}
		}
	]
}

module.exports = config
