const express = require('express')
const app = express()
const port = process.env.PORT || 3005

express.static('./dist')
app.use(express.static('dist'))


app.listen(port, () => {
	console.log(`Сервер запущен на ${port} порту!`)
})