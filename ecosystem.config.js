const config = {
	apps: [
		{
			name: 'donetsk-ru:3006',
			script: './index.js',
			error: './logs/pm2.err',
			instances: 1,
			autorestart: true,
			watch: false,
			max_memory_restart: '1G',
			env_production: {
				PORT: '3006',
				NODE_ENV: 'production'
			}
		}
	],
	deploy: {
		production: {
			user: 'dev',
			host: 'gromov.io',
			ref : 'origin/master',
			repo: 'git@gitlab.com:gromov_io/donetsk_ru.git',
			path: '/home/dev/donetsk_ru',
			'post-deploy' : `yarn && pm2 startOrRestart ecosystem.config.js --env production`
		},
		dev: {
			user: 'dev',
			host: 'gromov.io',
			ref : 'origin/dev',
			repo: 'git@gitlab.com:gromov_io/donetsk_ru.git',
			path: '/home/dev/dev_donetsk_ru',
			'post-deploy' : `yarn && pm2 startOrRestart dev.config.js --env production`
		}
	}
}

module.exports = config
